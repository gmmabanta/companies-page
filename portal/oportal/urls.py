from django.urls import path
from . import views

urlpatterns = [
    path('', views.opportunities, name='oportal-opportunities'),
]
