from django.shortcuts import render
# from .models import Opportunity


def opportunities(request):
    context = {
        # "opportunities": Opportunity.objects.all()
        "title": "Opportunities"
    }
    return render(request, "oportal/opportunities.html", context)
