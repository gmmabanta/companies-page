from django.db import models
from model_utils import Choices
from django.utils import timezone


# class Oppportunity(models.Model):
#     title = models.CharField(max_length=150)
#     # The company will eventually be a foreign key that will reference the Company model.
#     # company = models.ForeignKey(Company, on_delete=models.CASCADE)
#     company = models.CharField(max_length=150)
#     location = models.CharField(max_length=150)
#     duration = models.CharField(max_length=150)
#     date_posted = models.DateField(default=timezone.now)
#     description = models.TextField()
#     requirements = models.TextField()
#     CATEGORIES = Choices('Internship', 'Employment', 'Academe')
#     category = models.CharField(max_length=150, choices=CATEGORIES)
#     COURSES = Choices('Chemical Engineering', 'Civil Engineering', 'Computer Engineering', 'Computer Science',
#                       'Electronics and Communications Engineering', 'Electrical Engineering', 'Geodetic Engineering',
#                       'Industrial Engineering', 'Materials Engineering', 'Mechanical Engineering', 'Metallurgical Engineering',
#                       'Mining Engineering')
#     course = models.CharField(max_length=150, choices=COURSES)

#     # Magic method so each opportunity will appear based on its title.
#     def __str__(self):
#         return self.title
