from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from mycapes.forms import RegistrationForm
from mycapes.models import Student


class StudentAdmin(UserAdmin):
    list_display = ('student_number', 'email',
                    'first_name', 'last_name', 'is_admin')
    search_fields = ('email', 'student_number', 'first_name', 'last_name')
    readonly_fields = ('date_joined', 'last_login')

    ordering = ('date_joined',)

    filter_horizontal = ()
    list_filter = ()
    fieldsets = ()

    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('student_number', 'email', 'first_name', 'last_name', 'degree', 'program',  'graduation_year', 'password1', 'password2'),
        }),
    )


admin.site.register(Student, StudentAdmin)
