from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import authenticate
from mycapes.models import Student


class RegistrationForm(UserCreationForm):
    email = forms.EmailField(
        max_length=60)

    class Meta:
        model = Student
        fields = ("email", "student_number", "first_name", "last_name",
                  "graduation_year", "degree", "program", "password1", "password2")


class AutheticationForm(forms.ModelForm):
    password = forms.CharField(label="Password", widget=forms.PasswordInput)

    class Meta:
        model = Student
        fields = ("student_number", "password")

    def clean(self):
        if self.is_valid():
            student_number = self.cleaned_data['student_number']
            password = self.cleaned_data['password']
            if not authenticate(student_number=student_number, password=password):
                raise forms.ValidationError("Invalid Credentials")
