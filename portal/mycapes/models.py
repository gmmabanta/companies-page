import datetime
from django.db import models
from model_utils import Choices
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager


class StudentManager(BaseUserManager):
    def create_user(self, student_number, email, first_name, last_name, graduation_year, degree, program, password=None):
        if not student_number:
            raise ValueError("Users must enter a valid student number.")
        if not email:
            raise ValueError("Users must enter a email address.")
        if not first_name:
            raise ValueError("Users must enter their first name.")
        if not last_name:
            raise ValueError("Users must enter their last name.")
        if not graduation_year:
            raise ValueError(
                "Users must enter their expected year of graduation.")
        user = self.model(student_number=student_number,
                          email=self.normalize_email(email),
                          first_name=first_name,
                          last_name=last_name,
                          graduation_year=graduation_year,
                          degree=degree,
                          program=program)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, student_number, email, first_name, last_name, graduation_year, degree, program, password):
        user = self.create_user(student_number=student_number,
                                email=self.normalize_email(email),
                                first_name=first_name,
                                last_name=last_name,
                                graduation_year=graduation_year,
                                degree=degree,
                                program=program,
                                password=password)
        user.is_admin = True
        user.is_staff = True
        user.is_superuser = True
        user.save(using=self._db)
        return user


class Student(AbstractBaseUser):
    email = models.EmailField(verbose_name="email", max_length=60, unique=True)
    student_number = models.CharField(
        verbose_name="student number", max_length=9, unique=True)
    first_name = models.CharField(
        verbose_name="first name", max_length=60, blank=False)
    last_name = models.CharField(
        verbose_name="last name", max_length=60, blank=False)
    graduation_year = models.IntegerField(
        verbose_name="expected year of graduation", default=datetime.date.today().year)
    date_joined = models.DateTimeField(
        verbose_name="date joined", auto_now_add=True)
    last_login = models.DateTimeField(verbose_name="last login", auto_now=True)
    is_admin = models.BooleanField(verbose_name="admin", default=False)
    is_active = models.BooleanField(verbose_name="active", default=True)
    is_staff = models.BooleanField(verbose_name="staff", default=False)
    is_superuser = models.BooleanField(verbose_name="superuser", default=False)
    DEGREES = Choices("Bachelor of Arts", "Bachelor of Laws", "Bachelor of Science", "Certificate", "Diploma", "Doctor of Enginerring", "Doctor of Science", "Doctor of Philosophy", "Juris Doctor",
                      "Master of Arts", "Master of Engineering", "Master of Science", "Professional Masters")
    degree = models.CharField(
        max_length=60, choices=DEGREES, default=DEGREES.Diploma)
    PROGRAMS = Choices('Anthropology', 'Applied Mathematics', 'Applied Mathematics/Actuarial Science', 'Applied Physics', 'Applied Psychology', 'Archaeology', 'Architecture',
                       'Art Education', 'Art History', 'Art Studies', 'Asian Studies', 'Biology', 'Broadcast Communication', 'Building Technology', 'Business Administration',
                       'Business Administration and Accountancy', 'Business Economics', 'Business Economics (UPEPP)', 'Business Management (UPEPP)', 'Chemical Engineering',
                       'Chemistry', 'Chemical Education', 'Civil Engineering', 'Clothing Technology', 'Community Development', 'Communication Research', 'Community Nutrition',
                       'Community Development', 'Comparative Literature', 'Communication', 'Computer Engineering', 'Computer Science', 'Creative and Musical Performing Arts',
                       'Creative Writing', 'Demography', 'Development Economics', 'Early Childhood Development', 'Economics', 'Education', 'Electrical Engineering',
                       'Electronics Engineering', 'Elementary Education', 'English Studies: Language', 'English Studies: Literature', 'Environmental Engineering',
                       'Environmental Science', 'European Languages', 'Exercise and Sports Science', 'Family Life and Child Development', 'Filipino', 'Film', 'Finance',
                       'Fine Arts', 'Food Science', 'Food Technology', 'Geomatics Engineering', 'Geodetic Engineering', 'Geography', 'Geology', 'Hispanic Literature', 'History',
                       'Home Economics', 'Hotel, Restaurant and Institutional Management', 'Human Movement Science', 'Industrial Design', 'Industrial Engineering',
                       'Industrial Relations', 'Interior Design', 'International Studies', 'Islamic Studies', 'Journalism', 'Juris Doctor', 'Landscape Architecture',
                       'Librarianship', 'Library and Information Studies', 'Linguistics', 'Malikhaing Pagsulat', 'Management', 'Marine Science', 'Materials Engineering',
                       'Mathematics', 'Mechanical Engineering', 'Media Studies', 'Metallurgical Engineering', 'Meteorology', 'Microbiology', 'Mining Engineering',
                       'Molecular Biology and Biotechnology', 'Music',  'Nutrition', 'Painting', 'Philippine Studies', 'Philosophy', 'Physical Education', 'Physics',
                       'Political Science', 'Population Studies', 'Psychology', 'Public Administration', 'Public Management', 'Regional Development Planning', 'Sculpture',
                       'Secondary Education', 'Social Development', 'Social Work', 'Sociology', 'Speech Communication', 'Sports Science', 'Sports Studies', 'Statistics',
                       'Technology Management', 'Theatre Arts', 'Tourism', 'Tropical Landscape Architecture', 'Urban and Regional Planning', 'Visual Communication',
                       'Voluntary Sector Management', 'Women and Development', 'Zoology')
    program = models.CharField(
        max_length=60, choices=PROGRAMS, default=PROGRAMS.Anthropology)
    profile_picture = models.ImageField(verbose_name="profile picture",
                                        default="default.jpg", upload_to="profile_pictures")
    resume = models.FileField(upload_to="resumes", blank=True)

    USERNAME_FIELD = "student_number"
    REQUIRED_FIELDS = ['email', 'first_name', 'last_name',
                       'graduation_year', 'degree', 'program']

    objects = StudentManager()

    def __str__(self):
        return self.student_number

    def has_perm(self, perm, obj=None):
        return self.is_admin

    def has_module_perms(self, app_label):
        return True
