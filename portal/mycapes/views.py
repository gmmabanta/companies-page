from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from mycapes.forms import RegistrationForm, AutheticationForm


def register(request):
    if request.method == "POST":
        form = RegistrationForm(request.POST)
        if form.is_valid():
            form.save()
            student_number = form.cleaned_data.get("student_number")
            raw_password = form.cleaned_data.get("password1")
            student = authenticate(
                student_number=student_number, password=raw_password)
            login(request, student)
            return redirect("mycapes-dashboard")
    else:
        form = RegistrationForm()
    context = {
        "title": "Sign Up",
        "form": form
    }
    return render(request, 'mycapes/register.html', context)


def signout(request):
    logout(request)
    return redirect("portal-home")


def signin(request):
    if request.user.is_authenticated:
        return redirect("mycapes-dashboard")
    if request.POST:
        form = AutheticationForm(request.POST)
        if form.is_valid():
            student_number = request.POST['student_number']
            password = request.POST['password']
            student = authenticate(
                student_number=student_number, password=password)
            if student:
                login(request, student)
                return redirect("mycapes-dashboard")
    else:
        form = AutheticationForm()
    context = {
        "title": "Sign In",
        "form": form
    }
    return render(request, 'mycapes/login.html', context)


@login_required
def dashboard(request):
    context = {
        "title": "Dashboard"
    }
    return render(request, "mycapes/dashboard.html", context)


@login_required
def profile(request):
    context = {
        "title": "Profile",
        "student": request.user
    }
    return render(request, "mycapes/profile.html", context)
