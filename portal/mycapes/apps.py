from django.apps import AppConfig


class MycapesConfig(AppConfig):
    name = 'mycapes'
