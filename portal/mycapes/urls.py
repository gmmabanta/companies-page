from django.urls import path
from . import views

urlpatterns = [
    path('', views.dashboard, name='mycapes-dashboard'),
    path('profile/', views.profile, name='mycapes-profile'),
    path('register/', views.register, name='mycapes-register'),
    path('logout/', views.signout, name='mycapes-logout'),
    path('login/', views.signin, name='mycapes-login'),
]
