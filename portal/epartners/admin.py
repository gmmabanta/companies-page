from django.contrib import admin
from epartners.models import CompaniesModel

# Register your models here.

class CompanyAdmin(admin.ModelAdmin):
    list_display = ('company', 'representative', 'email_address', 'phone_number','address')
    
    search_fields = ('company', 'representative', 'website', 'fb', 'address')

    ordering = ('company','representative')

    filter_horizontal = ()
    list_filter = ('address',)
    fieldsets = (
        ('Company Information', {
            'fields': ('company','representative','email_address', 'phone_number',)
        }),
    )

    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('company', 'representative', 'email_address', 
                    'phone_number', 'website', 'fb', 'address'),
        }),
    )
    
admin.site.register(CompaniesModel, CompanyAdmin)
