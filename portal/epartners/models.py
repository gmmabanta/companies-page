from django.db import models

# Create your models here.

class CompaniesModel(models.Model):
    """A typical class defining a model, derived from the Model class."""
    company = models.CharField(max_length=50)
    representative = models.CharField(max_length=50)
    email_address= models.CharField(max_length=100)
    phone_number= models.CharField(max_length=15)
    website = models.CharField(max_length=200)
    fb = models.CharField(max_length=100)
    address = models.CharField(max_length=200)

    #Metadata
    class Meta:
        ordering = ['-company']

    #Methods 
    def __str__(self):
        """String for representing the Model object"""
        return f'{self.company}'
    def display_representative(self):
        """String to display representative in Admin."""
        return f'{self.representative}'
    
    """
    def get_absolute_url(self):
        #Returns the url to access a particular instance of CompaniesModel.
        return reverse('model-detail-view', args=[str(self.company)])
    
    """
objects = CompaniesModel()
