#Views file under epartners

from django.shortcuts import render
from django.views.generic import ListView
from epartners.models import CompaniesModel

# Create your views here.
class CompanyListView(ListView):
    model = CompaniesModel()
    template_name = "epartners/companies.html"
    #objs = CompaniesModel.objects.all()
    
    company_list = CompaniesModel.objects.all()
    """
    context = {
        'company': company_list.company,
        'representative': company_list.representative,
        'email_address': company_list.email_address,
        'phone_number': company_list.phone_number,
        'website': company_list.website,
        'fb': company_list.fb,
        'address': company_list.address
    }
    """
    context = {
        'company_list': company_list
    }
    def get(self, request):
        return render(request, "epartners/companies.html", context=self.context)
"""

    def post(self, request, *args, **kwargs):
        form = self.model(request.POST)
        return render(request, self.template_name, {'form': form})
"""
