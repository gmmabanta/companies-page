from django.contrib import admin
from django.urls import path, include
from . import views
from epartners.views import CompanyListView
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.home, name="portal-home"),
    path('mycapes/', include('mycapes.urls')),
    path('oportal/', include('oportal.urls')),
    path('company/', CompanyListView.as_view(), name='companies')
    
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL,
                          document_root=settings.MEDIA_ROOT)
